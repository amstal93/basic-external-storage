A simple NodeJS service that Reads & Writes to an external storage provider

> `GET /files`  
`POST /files`  
`GET /readiness`  

Used in Part 4 of an introductory blog series `Building a Web Service with Minikube` (in-progress)

>IMPORTANT: This example uses Backblaze B2 Cloud Storage API. For this example to work you will need an Application Key ID & Application Key, and insert them in the `.env` & `_kubernetes/basic-external-storage.yaml`.

#### To run locally 

`npm start`

Navigate to `localhost:3000/files`

#### To run w/ Docker

Build Image & run container  
`docker build -t basic-external-storage .`  
`docker run -p 3000:3000 --env-file=.env -d basic-external-storage`

#### To run w/ Kubernetes

`cd` into the `_kubeManifests` directory & apply the current context  
`$ kk apply -f .`

#### Resources
* https://www.backblaze.com/b2/docs/quick_account.html
* https://www.backblaze.com/b2/docs/
* https://github.com/BretFisher/node-docker-good-defaults/blob/master/Dockerfile
* https://github.com/BretFisher/node-docker-good-defaults/blob/69c923bc646bc96003e9ada55d1ec5ca943a1b19/bin/www
* https://github.com/RisingStack/kubernetes-graceful-shutdown-example/blob/master/src/index.js
