ARG NODE_VERSION=14.3.0-alpine3.11
ARG PORT=3000

# Stage 1: install dependencies
FROM node:$NODE_VERSION AS base

ENV NODE_ENV=production

RUN mkdir /opt/node_app && chown node:node /opt/node_app
WORKDIR /opt/node_app

COPY package*.json ./

# use only package-lock.json file
RUN npm ci && npm cache clean --force


# Stage 2: run service
FROM base AS prod

EXPOSE $PORT

WORKDIR /opt/node_app

COPY . .

USER node
CMD ["node", "./bin/www"]
