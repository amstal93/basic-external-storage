const getAuthCreds = require('../endpoints/backblaze').getAuthCreds;


function readinessProbe(req, res) {
  const authCreds = getAuthCreds();
  const { authorizationToken } = authCreds;
  res.statusCode = 500;

  if (authorizationToken) res.statusCode = 200;

  console.info(res.statusCode);

  res.end();
}


module.exports = readinessProbe;
