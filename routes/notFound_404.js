const invalidRequest = (method, url) => {
  return {
    requestMethod: method,
    requestUrl: url,
    allowedRequests: [
      `GET /files`,
      `POST /files`,
      `GET /readiness`
    ]
  };
};

function notFound_404(req, res) {
  const { method, url } = req;
  const resBody = JSON.stringify(invalidRequest(method, url));

  res.writeHead(404, {
    'Content-Type': 'text/json',
    'Content-Length': Buffer.byteLength(resBody)
  });
  res.write(resBody);
  res.end();
}


module.exports = notFound_404;
