const listFiles = require('../endpoints/backblaze').listFiles;


async function getFiles(req, res) {
  const files = await listFiles();
  const fileNames = files.map(file => {
    const { fileId, fileName } = file;
    return { fileId, fileName };
  });

  res.writeHead(200, {
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(JSON.stringify(fileNames))
  });

  res.write(JSON.stringify(fileNames));
  res.end();
}


module.exports = getFiles;
