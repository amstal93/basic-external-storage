const getFiles = require('./GET_@files');
const postFiles = require('./POST_@files');
const readinessProbe = require('./readinessProbe');
const notFound_404 = require('./notFound_404');


const allowedMethods = ['GET', 'POST'];
const allowedPaths = ['/files', '/readiness'];
const allowedGETpaths = ['/files', '/readiness'];
const allowedPOSTpaths = ['/files'];

function routes() {
  return (req, res) => {
    const { method, url } = req;

    console.group('Route Handler');
    console.info({ method, url });
    console.groupEnd();

    if (!allowedMethods.includes(method) || !allowedPaths.includes(url) ||
        (method === 'GET' && !allowedGETpaths.includes(url)) ||
        (method === 'POST' && !allowedPOSTpaths.includes(url))
    ) notFound_404(req, res);

    if (method === 'GET' && url === '/files') getFiles(req, res);

    if (method === 'POST' && url === '/files') postFiles(req, res);

    if (method === 'GET' && url === '/readiness') readinessProbe(req, res);
  };
}


module.exports = routes;
